package gocipher

import (
	"bytes"
	"testing"
)

func TestCaesar(t *testing.T) {
	testCases := []struct {
		p   []byte
		c   []byte
		key int
	}{
		{
			[]byte("THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG"),
			[]byte("UIF RVJDL CSPXO GPY KVNQT PWFS UIF MBAZ EPH"),
			1,
		},
		{
			[]byte("THEQUICKBROWNFOXJUMPSOVERTHELAZYDOG"),
			[]byte("UIFRVJDLCSPXOGPYKVNQTPWFSUIFMBAZEPH"),
			1,
		},
	}

	for _, tc := range testCases {
		if c := CaesarEncrypt(tc.p, tc.key); !bytes.Equal(c, tc.c) {
			t.Fatalf("Encrypt failed: got %s, want %s", c, tc.c)
		}

		if c := CaesarDecrypt(tc.c, tc.key); !bytes.Equal(c, tc.p) {
			t.Fatalf("Decrypt failed: got %s, want %s", c, tc.p)
		}
	}

}

func BenchmarkCaesarEncrypt(b *testing.B) {
	pt := []byte("THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG")
	for i := 0; i < b.N; i++ {
		CaesarEncrypt(pt, 1)
	}
}

func BenchmarkCaesarDecrypt(b *testing.B) {
	ct := []byte("UIF RVJDL CSPXO GPY KVNQT PWFS UIF MBAZ EPH")
	for i := 0; i < b.N; i++ {
		CaesarDecrypt(ct, 1)
	}
}
