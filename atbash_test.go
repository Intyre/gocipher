package gocipher

import (
	"bytes"
	"testing"
)

func TestAtbash(t *testing.T) {
	testCases := []struct {
		p []byte
		c []byte
	}{
		{
			[]byte("THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG"),
			[]byte("GSV JFRXP YILDM ULC QFNKH LEVI GSV OZAB WLT"),
		},
		{
			[]byte("THEQUICKBROWNFOXJUMPSOVERTHELAZYDOG"),
			[]byte("GSVJFRXPYILDMULCQFNKHLEVIGSVOZABWLT"),
		},
	}

	for _, tc := range testCases {
		if c := AtbashEncrypt(tc.p); !bytes.Equal(c, tc.c) {
			t.Fatalf("Encrypt failed: got %s, want %s", c, tc.c)
		}

		if c := AtbashDecrypt(tc.c); !bytes.Equal(c, tc.p) {
			t.Fatalf("Decrypt failed: got %s, want %s", c, tc.p)
		}
	}

}

func BenchmarkAtbashEncrypt(b *testing.B) {
	pt := []byte("THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG")
	for i := 0; i < b.N; i++ {
		AtbashEncrypt(pt)
	}
}

func BenchmarkAtbashDecrypt(b *testing.B) {
	ct := []byte("GSV JFRXP YILDM ULC QFNKH LEVI GSV OZAB WLT")
	for i := 0; i < b.N; i++ {
		AtbashDecrypt(ct)
	}
}
