package gocipher

func btoi(b byte) int {
	return int(b) - 65
}

func itob(i int) byte {
	const n int = 26
	i = ((i % n) + n) % n
	return byte(i + 65)
}

func isAlpha(b byte) bool {
	return b >= 'A' && b <= 'Z'
}
