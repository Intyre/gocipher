test:
	go test -v

bench:
	go test -v -run=x -bench=. --benchmem

doc:
	godoc --http=":8080"

conv:
	goconvey -host 0.0.0.0 -port 8081 -launchBrowser=false

t: test # alias to test
b: bench # alias to bench

