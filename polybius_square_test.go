package gocipher

import (
	"bytes"
	"fmt"
	"testing"
)

func TestPolybiusSquare(t *testing.T) {
	testCases := []struct {
		p     []byte
		c     []byte
		key   []byte
		size  int
		chars []byte
	}{
		{
			[]byte("DEFENDTHEEASTWALLOFTHECASTLE"),
			[]byte("KUBSKSBSBBKUSSBUBSBSMMUUSSMUMMKKKKSKKSSSBUBSMSMMUUSSKKBS"),
			[]byte("APCZWRLFBDKOTYUQGENHXMIVS"),
			5,
			[]byte("MKSBU"),
		},
	}

	for _, tc := range testCases {
		ps := NewPolybiusSquare(tc.key, tc.size, tc.chars)

		if ct := ps.Encrypt(tc.p); !bytes.Equal(ct, tc.c) {
			fmt.Printf("Encrypt: got %s, want %s\n", ct, tc.c)
		}

		if pt := ps.Decrypt(tc.c); !bytes.Equal(pt, tc.p) {
			fmt.Printf("Decrypt: got %s, want %s\n", pt, tc.p)
		}
	}

}

func BenchmarkPolybiusSquareEncrypt(b *testing.B) {
	pt := []byte("DEFENDTHEEASTWALLOFTHECASTLE")
	key := []byte("APCZWRLFBDKOTYUQGENHXMIVS")
	size := 5
	chars := []byte("MKSBU")
	for i := 0; i < b.N; i++ {
		NewPolybiusSquare(key, size, chars).Encrypt(pt)
	}
}

func BenchmarkPolybiusSquareDecrypt(b *testing.B) {
	ct := []byte("KUBSKSBSBBKUSSBUBSBSMMUUSSMUMMKKKKSKKSSSBUBSMSMMUUSSKKBS")
	key := []byte("APCZWRLFBDKOTYUQGENHXMIVS")
	size := 5
	chars := []byte("MKSBU")
	for i := 0; i < b.N; i++ {
		NewPolybiusSquare(key, size, chars).Decrypt(ct)
	}
}
