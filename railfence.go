package gocipher

import (
	"bytes"
)

// RailfenceEncrypt ...
func RailfenceEncrypt(pt []byte, key int) (ct []byte) {
	pt = bytes.ToUpper(pt)
	ct = make([]byte, len(pt))

	for k, v := range buildFence(len(pt), key) {
		ct[k] = pt[v]
	}
	return

}

// RailfenceDecrypt ...
func RailfenceDecrypt(ct []byte, key int) (pt []byte) {
	ct = bytes.ToUpper(ct)
	pt = make([]byte, len(ct))

	for k, v := range buildFence(len(ct), key) {
		pt[v] = ct[k]
	}
	return
}

func buildFence(size int, key int) []int {
	rails := make([][]int, key)

	rail, dRail := 0, 1
	for i := 0; i < size; i++ {
		if i < key {
			row := make([]int, 0, size)
			for r := range row {
				row[r] = -1
			}
			rails[i] = row
		}

		rails[rail] = append(rails[rail], i)
		if rail+dRail < 0 || key <= rail+dRail {
			dRail = -dRail
		}
		rail += dRail
	}

	result := make([]int, 0, size)
	for _, slice := range rails {
		for _, v := range slice {
			if v == -1 {
				continue
			}
			result = append(result, v)
		}
	}
	return result
}
