package gocipher

import "bytes"

// ROT13Encrypt ...
func ROT13Encrypt(pt []byte) (ct []byte) {
	ct = bytes.ToUpper(pt)

	for i, char := range ct {
		if isAlpha(char) {
			ct[i] = itob(btoi(char) + 13)
		}
	}
	return
}

// ROT13Decrypt ...
func ROT13Decrypt(ct []byte) (pt []byte) {
	pt = bytes.ToUpper(ct)

	for i, char := range ct {
		if isAlpha(char) {
			pt[i] = itob(btoi(char) + 13)
		}
	}
	return
}
