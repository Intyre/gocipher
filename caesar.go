package gocipher

import (
	"bytes"
)

// CaesarEncrypt ...
func CaesarEncrypt(pt []byte, key int) (ct []byte) {
	ct = bytes.ToUpper(pt)

	for i, char := range ct {
		if isAlpha(char) {
			ct[i] = itob(btoi(char) + key%26)
		}
	}
	return
}

// CaesarDecrypt ...
func CaesarDecrypt(ct []byte, key int) (pt []byte) {
	pt = bytes.ToUpper(ct)

	for i, char := range pt {
		if isAlpha(char) {
			pt[i] = itob(btoi(char) - key%26)
		}
	}
	return
}
