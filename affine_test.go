package gocipher

import (
	"bytes"
	"testing"
)

func TestAffine(t *testing.T) {
	testCases := []struct {
		p    []byte
		c    []byte
		a, b int
	}{
		{
			[]byte("THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG"),
			[]byte("ASD LFXTH OQBPW IBU CFRGV BKDQ ASD MJEZ YBN"),
			5, 9,
		},
		{
			[]byte("THEQUICKBROWNFOXJUMPSOVERTHELAZYDOG"),
			[]byte("ASDLFXTHOQBPWIBUCFRGVBKDQASDMJEZYBN"),
			5, 9,
		},
	}

	for _, tc := range testCases {
		if c := AffineEncrypt(tc.p, tc.a, tc.b); !bytes.Equal(c, tc.c) {
			t.Fatalf("Encrypt failed: got %s, want %s", c, tc.c)
		}

		if c := AffineDecrypt(tc.c, tc.a, tc.b); !bytes.Equal(c, tc.p) {
			t.Fatalf("Decrypt failed: got %s, want %s", c, tc.p)
		}
	}

}

func BenchmarkAffineEncrypt(b *testing.B) {
	pt := []byte("THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG")
	for i := 0; i < b.N; i++ {
		AffineEncrypt(pt, 5, 9)
	}
}

func BenchmarkAffineDecrypt(b *testing.B) {
	ct := []byte("ASD LFXTH OQBPW IBU CFRGV BKDQ ASD MJEZ YBN")
	for i := 0; i < b.N; i++ {
		AffineDecrypt(ct, 5, 9)
	}
}
